use movies;

-- 1

--select title, name
--from movie join movieexec on producerc# = cert#
--where producerc# = (select producerc#
--				from movie
--				where title = 'Star Wars')


-- 2

--select distinct name
--from starsin join (movie join movieexec on producerc# = cert#) on movietitle = title and movieyear = year
--where starname = 'Harrison Ford' 

-- 3

--select distinct name, starname
--from starsin join movie on movietitle = title and movieyear = year
--			join studio on studioname = name
--order by name 


-- 4

--select starname, networth, title
--from starsin join movie on movietitle = title and movieyear = year
--			join movieexec on producerc# = cert#
--where networth >= all(select networth
--						from movieexec)

-- 5

--select name, movietitle
--from moviestar left join starsin on starname = name
--where starname is null 

use pc;

-- 1

--select product.maker, product.model, product.type
--from product left join pc on product.model = pc.model
--			left join printer on product.model = printer.model
--			left join laptop on product.model = laptop.model
--where pc.model is null and printer.model is null and laptop.model is null

-- 2

--(select maker
--from product as pr join laptop as lp on pr.model = lp.model)
--intersect
--(select maker
--from product as pr join printer on pr.model = printer.model)

-- 3 - NZ

select *
from laptop 


-- 4 - NA

use ships;


-- 1

--select *
--from ships left join classes on ships.class = classes.class

-- 2 

--select *
--from ships right join classes on ships.class = classes.class

-- 3

--select country, name
--from ships join classes on ships.class = classes.class
--			left join outcomes on name = ship
--where ship is null
--order by country

-- 4

--select name as "Ship Name"
--from ships join classes on ships.class = classes.class
--where numguns >= 7 and launched = 1916

-- 5

--select ship, battle, date
--from outcomes join battles on battle = name
--where result = 'Sunk'
--order by battle


-- 6

--select name, displacement, launched 
--from ships join classes on ships.class = classes.class
--where ships.name = ships.class

-- 7

--select classes.class, type, country, numguns, bore, displacement
--from classes left join ships on classes.class = ships.class
--where launched is null

-- 8

--select name, displacement, numguns, result
--from ships join outcomes on name = ship and battle = 'North Atlantic'
--			join classes on ships.class = classes.class
			







