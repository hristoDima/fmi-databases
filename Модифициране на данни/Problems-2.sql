USE movies;
-- 
-- -- 1 --
-- -- INSERT INTO moviestar (name, birthdate) VALUES ('Nicole Kidman', '1967-06-20')
-- 
-- -- 2 --
-- DELETE
-- FROM movieexec
-- WHERE networth < 30000000;
-- 
-- -- 3 --
-- DELETE
-- FROM moviestar
-- WHERE address IS NULL;
--
USE pc;
--
-- -- 4 --
-- INSERT INTO pc VALUES (12, '1100', 2400, 2048, 500, '52x', 299);
-- INSERT INTO product (maker, model, TYPE) VALUES ('C', '1100', 'PC')
-- 
-- -- 5 --
-- DELETE
-- FROM pc
-- WHERE model = '1100'
-- 
-- -- 6 --
-- DELETE
-- FROM laptop
-- WHERE model IN (
-- SELECT DISTINCT model
-- FROM product
-- WHERE TYPE = 'Laptop' AND maker NOT IN (
-- SELECT maker
-- FROM product
-- WHERE TYPE = 'Printer')
-- )
-- 
-- -- 7 --
-- UPDATE product SET maker = 'A'
-- WHERE maker = 'B'
-- 
-- -- 8 --
-- UPDATE pc SET price = price/2, hd = hd+20;
-- 
-- -- 9 --
-- UPDATE laptop SET screen = screen+1
-- WHERE model IN (
-- SELECT model
-- FROM product
-- WHERE maker = 'B' AND TYPE = 'Laptop')
-- 
--
USE ships;

-- 10 --
-- INSERT INTO classes VALUES ('Nelson', 'bb', 'Gt. Britain', 9, 16, 34000);
-- INSERT INTO ships VALUES ('Nelson', 'Nelson', 1927), ('Rodney', 'Nelson', 1927);


-- 11 --
-- DELETE
-- FROM ships
-- WHERE name IN (
-- SELECT ship
-- FROM outcomes
-- WHERE result = 'sunk')
-- 

-- 12 --
-- UPDATE classes SET bore = bore * 2.5, displacement = displacement * 0.9;






