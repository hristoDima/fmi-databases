use movies;

-- 1
INSERT INTO MOVIESTAR (NAME, BIRTHDATE)
VALUES ('Nicole Kidman', '1967-06-20')

-- 2
DELETE FROM movieexec
where networth < 30000000

--   3
delete from MOVIESTAR
where address is null


use pc;

-- 4
INSERT INTO product
VALUES ('C','1100','PC')

INSERT INTO pc (code, model, speed, ram, hd, cd, price)
VALUES (12, '1100', 2400, 2048, 500, '52', 299);

-- 5
delete from pc
where model = 1100

--6
delete from laptop
where model in (
                    select model
                    from product
                    where type = 'Laptop' and maker not in (
                                                        select maker
                                                        from product
                                                        where type = 'Printer'
                                                      )
                    )

--7
update product
set maker = 'A'
where maker ='B'

--8
update pc
set price = price/2, hd = hd+20

-- 9 ??????? vqrno e, no ne raboti :/
update laptop
set screen = screen + 1
where model in (select product.model
                from product
                where product.maker = 'B' and type='Laptop'
                );


use ships;

--10
insert into CLASSES
values ('Nelson', 'bb', 'Gt.Britain', 9, 16, 34000)

insert into ships
values ('Nelson', 'Nelson', 1927),
       ('Rodney', 'Nelson', 1927)

--11
delete from ships
where name in (select ship from outcomes where result = 'sunk')

--12
update classes
set bore = bore * 2.25, displacement = displacement * (1/1.1)