USE flights;

-- 1 -- ?????
-- CREATE VIEW v_1 (airline, fnumber, reserved_seats) AS
-- SELECT airlines.name, flight_number, SUM(STATUS)
-- FROM bookings
-- JOIN airlines ON bookings.airline_code = airlines.code
-- GROUP BY flight_number, airlines.name
-- ;
-- SELECT *
-- FROM v_1;
-- 
-- DROP VIEW v_1;

-- 2 -- ????
-- CREATE VIEW v_2 AS
-- SELECT agencies.name,
-- FROM (agencies
-- LEFT JOIN bookings ON bookings.agency = agencies.name)
-- LEFT JOIN customers ON customer_id = customers.id
-- GROUP BY agencies.name
-- 


-- 3 --
-- 
-- CREATE VIEW v_3 AS
-- SELECT *
-- FROM agencies
-- WHERE city='sofia';
-- SELECT *
-- FROM v_3;
-- 
-- DROP VIEW v_3;
-- 
-- -- 4 --
-- CREATE VIEW v_4 AS
-- SELECT *
-- FROM agencies
-- WHERE phone IS NULL WITH CHECK OPTION;
-- SELECT *
-- FROM v_4;
-- 
-- -- 5 --
-- INSERT INTO v_3
-- VALUES('T1 Tour', 'Bulgaria', 'Sofia','+359');
-- INSERT INTO v_4
-- VALUES('T2 Tour', 'Bulgaria', 'Sofia',null);
-- INSERT INTO v_3
-- VALUES('T3 Tour', 'Bulgaria', 'Varna','+359');
-- INSERT INTO v_4
-- VALUES('T4 Tour', 'Bulgaria', 'Varna',null);
-- INSERT INTO v_4
-- VALUES('T4 Tour', 'Bulgaria', 'Sofia','+359');
-- 
-- 
USE movies;

-- 6 --
-- CREATE VIEW RichExec AS
-- SELECT name, address, cert#, networth
-- FROM movieexec
-- WHERE networth >= 10000000;
-- SELECT *
-- FROM richexec;
-- 
-- 7 --













