USE MASTER;
-- CREATE DATABASE university; 
USE university;

-- 1 --

-- a --
-- CREATE TABLE product(
-- maker CHAR(1),
-- model CHAR(4),
-- type VARCHAR(7)
-- );
-- CREATE TABLE printer(
-- code INT,
-- model CHAR(4),
-- price DECIMAL(8,2)
-- );
-- 
-- -- b --
-- INSERT INTO product ('A', '1111', 'printer');
-- INSERT INTO printer (1, '1111', 399.99);
-- 
-- -- c --
-- ALTER TABLE printer ADD TYPE VARCHAR(6),
-- color CHAR(1) NOT NULL DEFAULT 'n';
-- 
-- -- d --
-- ALTER TABLE
-- DROP COLUMN price;

-- -- e --
-- DROP TABLE printer, product;

-- 2 --
 
-- a --
-- CREATE TABLE users(
-- id INT identity(1,1) NOT NULL CONSTRAINT pk_users PRIMARY KEY,
-- email VARCHAR(20) NOT NULL, PASSWORD VARCHAR(25) NOT NULL,
-- reg_date DATE
-- );
-- CREATE TABLE friends(
-- friend_1 INT NOT NULL,
-- friend_2 INT NULL
-- );
-- CREATE TABLE walls(
-- reciever INT NOT NULL,
-- sender INT NOT NULL,
-- message VARCHAR(1000) NOT NULL,
-- date_send DATE
-- );
-- CREATE TABLE groups(
-- id INT identity(1,1) NOT NULL CONSTRAINT pk_groups PRIMARY KEY,
-- name VARCHAR(30) NULL DEFAULT 'unknown group',
-- info VARCHAR(50) NULL DEFAULT ' '
-- );

USE MASTER;
-- DROP DATABASE university;









