use flights;

-- 1 --
-- alter table flights add num_pass int null;
-- 2 --
-- alter table agencies add num_book int null;

-- 3 --
-- CREATE TRIGGER tr_bk ON bookings AFTER
-- INSERT AS
-- UPDATE flights SET num_pass = num_pass+1
-- WHERE fnumber = (
-- SELECT flight_number
-- FROM inserted)
-- UPDATE agencies SET num_book = num_book+1
-- WHERE agencies.name = (
-- SELECT agency
-- FROM inserted);
-- 

-- 4 --
-- CREATE TRIGGER tr_4 ON bookings AFTER
-- DELETE AS
-- UPDATE flights SET num_pass = num_pass-1
-- WHERE fnumber = (
-- SELECT flight_number
-- FROM deleted)
-- UPDATE agencies SET num_book = num_book-1
-- WHERE name = (
-- SELECT agency
-- FROM deleted);
-- 

-- 5 -- MURZI ME
-- create trigger tr_5
-- on bookings
-- after update
-- 









