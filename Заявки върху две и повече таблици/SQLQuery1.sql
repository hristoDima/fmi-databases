use movies;

--select name
--from starsin, moviestar
--where moviestar.name = starsin.starname AND gender = 'M' AND MOVIETITLE = 'The Usual Suspects'

--select starname
--from movie join STARSIN on title = MOVIETITLE and year = movieyear
--where MOVIEYEAR = 1995 and studioname = 'MGM' 

--select distinct movieexec.name 
--from movie join movieexec on PRODUCERC# = CERT#
--where studioname = 'MGM'

--select m2.title 
--from movie as m1, movie as m2
--where m1.title = 'Star Wars' and m2.length > m1.length

--select m2.name
--from movieexec as m1, movieexec as m2
--where m1.name = 'Stephen Spielberg' and m2.networth > m1.networth


--select title
--from movie, movieexec as m1, movieexec as m2
--where m1.name = 'Stephen Spielberg' and m2.networth > m1.networth and PRODUCERC# = m2.CERT#

--select title
--from movie join movieexec as m1 on PRODUCERC#=CERT#, movieexec as m2
--where m2.name = 'Stephen Spielberg' and m1.networth > m2.networth

use pc;

--select maker, speed 
--from product join laptop on product.model = laptop.model
--where hd > 9


-- -------2----------

--(select product.model, price
--from product join pc on product.model = pc.model
--where maker = 'B')
--union
--(select product.model, price
--from product join laptop on product.model = laptop.model
--where maker = 'B')
--union 
--(select product.model, price
--from product join printer on product.model = printer.model
--where maker = 'B')

-- ----3------
--(select maker 
--from product
--where type = 'Laptop')
--except
--(select maker 
--from product
--where type = 'PC')

-- -----4-------
--select distinct pc1.hd
--from pc as pc1, pc as pc2
--where pc1.hd = pc2.hd and pc1.code != pc2.code

-- --------5------ nZ
--select pc1.model, pc2.model
--from pc as pc1, pc as pc2
--where pc1.speed = pc2.speed and pc1.hd = pc2.hd and pc1.model > pc2.model 

-- -------6------
--select distinct pr1.maker
--from pc as pc1 join product as pr1 on pr1.model = pc1.model, 
--	pc as pc2 join product as pr2 on pr2.model = pc2.model
--where pc1.speed >= 400 and pr1.maker = pr2.maker and pc1.code != pc2.code

use ships;

-- 1
--select name
--from ships join classes on ships.class = classes.class
--where displacement > 50000

-- 2
--select ships.name, displacement, numguns
--from ships join  classes on ships.class = classes.class
--		 join outcomes on ships.name = outcomes.ship
--where outcomes.battle = 'Guadalcanal'

-- 3
--select c1.country
--from classes as c1, classes as c2
--where c1.type = 'bb' and c2.type = 'bc' and c1.country = c2.country

-- 4
--select distinct o1.ship
--from outcomes as o1 
--			join battles as b1 on o1.battle = b1.name,
--	outcomes as o2 
--			join battles as b2 on o2.battle = b2.name
--where o1.result = 'damaged' and o2.result = 'ok' and b1.date < b2.date and o1.ship = o2.ship

