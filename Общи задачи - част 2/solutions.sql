USE MASTER;
-- create database FurnitureCompany;
USE FurnitureCompany;

-- 1 --

-- CREATE TABLE product(
-- id INT NOT NULL,
-- description VARCHAR(255),
-- finish VARCHAR(255) CHECK(finish IN ('череша','естествен ясен', 'бял ясен', 'червен дъб', 'естествен дъб', 'орех')),
-- price DECIMAL(8,2), CONSTRAINT pk_product PRIMARY KEY(id)
-- );
-- --
-- CREATE TABLE customer(
-- id INT NOT NULL identity(1,1) CONSTRAINT pk_customer PRIMARY KEY,
-- name VARCHAR(50) NOT NULL,
-- address VARCHAR(50) NULL,
-- city VARCHAR(50) NULL,
-- city_code VARCHAR(10) NULL
-- );
-- --
-- CREATE TABLE order_t(
-- id INT NOT NULL, DATE DATE,
-- customer_id INT NOT NULL CONSTRAINT fk_cust REFERENCES customer
-- );
-- --
-- CREATE TABLE order_line(
-- order_id INT NOT NULL,
-- product_id INT NOT NULL,
-- order_quantity INT NOT NULL, CONSTRAINT fk_product FOREIGN KEY (product_id) REFERENCES product(id)
-- );
-- --
-- ALTER TABLE order_t ADD CONSTRAINT pk_order_t PRIMARY KEY(id); ALTER TABLE order_line ADD CONSTRAINT fk_oder FOREIGN KEY(order_id) REFERENCES order_t(id); ALTER TABLE product ADD product_line_id INT NOT NULL;
-- INSERT INTO CUSTOMER VALUES
-- ('Иван Петров', 'ул. Лавеле 8', 'София', '1000'),
-- ('Камелия Янева', 'ул. Иван Шишман 3', 'Бургас', '8000'),
-- ('Васил Димитров', 'ул. Абаджийска 87', 'Пловдив', '4000'),
-- ('Ани Милева', 'бул. Владислав Варненчик 56', 'Варна','9000');
-- --
-- INSERT INTO PRODUCT VALUES
-- (1000, 'офис бюро', 'череша', 195, 10),
-- (1001, 'директорско бюро', 'червен дъб', 250, 10),
-- (2000, 'офис стол', 'череша', 75, 20),
-- (2001, 'директорски стол', 'естествен дъб', 129, 20),
-- (3000, 'етажерка за книги', 'естествен ясен', 85, 30),
-- (4000, 'настолна лампа', 'естествен ясен', 35, 40);
-- --
-- INSERT INTO ORDER_T VALUES
-- (100, '2013-01-05', 1),
-- (101, '2013-12-07', 2),
-- (102, '2014-10-03', 3),
-- (103, '2014-10-08', 2),
-- (104, '2015-10-05', 1),
-- (105, '2015-10-05', 4),
-- (106, '2015-10-06', 2),
-- (107, '2016-01-06', 1);
-- --
-- INSERT INTO ORDER_LINE VALUES
-- (100, 4000, 1),
-- (101, 1000, 2),
-- (101, 2000, 2),
-- (102, 3000, 1),
-- (102, 2000, 1),
-- (106, 4000, 1),
-- (103, 4000, 1),
-- (104, 4000, 1),
-- (105, 4000, 1),
-- (107, 4000, 1);
-- --

-- 2 --
-- SELECT product.id, description, COUNT(product.id) AS times_ordered
-- FROM product
-- JOIN order_line ON product_id = product.id
-- GROUP BY product.id, description

-- 3 --
-- SELECT product.id, description, SUM(order_quantity)
-- FROM product
-- LEFT JOIN order_line ON product_id = product.id
-- GROUP BY product.id, description

-- 4 --
-- SELECT name, SUM(price * order_quantity)
-- FROM customer
-- JOIN order_t ON customer.id = customer_id
-- JOIN order_line ON order_t.id = order_id
-- JOIN product ON product_id = product.id
-- GROUP BY name
-- 

USE pc;

-- 5.1 --
-- SELECT DISTINCT p1.maker
-- FROM product AS p1, product AS p2
-- WHERE p1.type = 'printer' AND p2.type = 'laptop' AND p1.maker = p2.maker

-- 5.2 --
-- SELECT distinct maker
-- FROM product
-- WHERE TYPE = 'laptop' AND maker IN (
-- SELECT maker
-- FROM product
-- WHERE TYPE='printer');
-- 

-- 6 -- ??
-- update pc
-- set price = price * 0.95
-- where model in (select pc.model
-- from pc join product on pc.model = product.model
-- where
-- )
-- 

-- 7 --
-- SELECT hd, MIN(price)
-- FROM pc
-- WHERE hd>=10 AND hd<=30
-- GROUP BY hd
-- 

use ships;

-- 8 a--
-- CREATE VIEW v_8 (battle) AS
-- SELECT DISTINCT outcomes.battle
-- FROM battles
-- JOIN outcomes ON battles.name = outcomes.battle
-- GROUP BY outcomes.battle
-- HAVING COUNT(ship) > ALL (
-- SELECT COUNT(*)
-- FROM outcomes
-- WHERE battle = 'Guadalcanal');
-- 
-- SELECT *
-- FROM v_8;
-- 
-- DROP VIEW v_8;
-- 

-- 8 b-- ??

-- CREATE VIEW v_8_2 (battle) AS
-- SELECT DISTINCT outcomes.battle
-- FROM battles
-- JOIN outcomes ON battles.name = outcomes.battle
-- GROUP BY outcomes.battle
-- HAVING COUNT(ship) > ALL (
-- SELECT COUNT(*)
-- FROM outcomes
-- WHERE battle = 'Guadalcanal');
-- 

-- 9 --
-- DELETE
-- FROM outcomes
-- WHERE battle IN (
-- SELECT battle
-- FROM outcomes
-- GROUP BY battle
-- HAVING COUNT(*) = 1);

-- 10 --
-- INSERT INTO outcomes VALUES ('Missouri','Surigao Strait', 'sunk'),
-- ('Missouri','North Cape', 'sunk'),
-- ('Missouri','North Atlantic', 'ok');
-- 
-- DELETE
-- FROM outcomes
-- WHERE result = 'sunk' AND ship IN (
-- SELECT ship
-- FROM outcomes
-- WHERE result='sunk'
-- GROUP BY ship
-- HAVING COUNT(*)>=2);
-- 

-- 11 -- ??
-- CREATE VIEW v_11 (battle, country) AS
-- SELECT distinct battles.name, classes.country
-- FROM battles
-- JOIN outcomes ON battles.name = outcomes.battle
-- JOIN ships ON outcomes.ship = ships.name
-- JOIN classes ON ships.class = classes.class
-- ;
-- SELECT *
-- FROM v_11;
-- 
-- drop view v_11;
-- 
-- select distinct battle
-- from v_11
-- group by battle
-- having country in (select country
-- from v_11
-- 						where battle='Guadalcanal');


