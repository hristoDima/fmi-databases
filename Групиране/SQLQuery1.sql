use pc;

-- 1

--select convert(decimal(10, 2), AVG(speed)) as speed
--from pc

-- 2

--select maker, AVG(screen) as avgScreen
--from laptop join product on laptop.model = product.model
--group by maker

-- 3

--select convert(decimal(6, 2), AVG(speed)) as AvgSpeed
--from laptop
--where price>1000

-- 4 !!!!!! 

--select maker, avg(price)
--from pc join product on pc.model = product.model
--where maker = 'A'
--group by maker


-- 5 !!!!!

--select avg(price)
--from (
--(select maker, price
--from pc join product on pc.model = product.model
--where maker='B')
--union all 
--(select maker, price
--from laptop join product on laptop.model = product.model
--where maker='B')
--) as b
--group by (maker)

-- 6

--select speed, AVG(price)
--from pc
--group by speed

-- 7

--select maker, count(code) as number_of_pc
--from pc join product on pc.model = product.model
--group by maker
--having count(code) >= 3

-- 8 ---slojno

--select maker, t1.MaxPrice
--from
--(select maker, max(price) as MaxPrice
--from pc join product on pc.model = product.model
--group by maker) as t1
--join
--(select max(price) as MaxPrice
--from pc) as t2
--on t1.MaxPrice = t2.MaxPrice

--group by maker
--having MaxPrice = max(MaxPrice)


-- 9

--select speed, AVG(price) as avgPrice
--from pc
--group by speed
--having speed > 800

-- 10 

--select distinct t1.maker, avgHDD
--from
--(select maker, convert(decimal(10, 2), AVG(hd)) as avgHDD
--from pc join product on pc.model = product.model
--group by maker) as t1
--join
--(select maker
--from printer join product on printer.model = product.model) as t2
--on t1.maker = t2.maker

use ships;

-- 1 

--select count(class) as classes_number
--from classes
--where type = 'bb'

-- 2 

--select classes.class, AVG(numguns)
--from ships full join classes on ships.class = classes.class 
--where type = 'bb'
--group by classes.class

-- 3 - ne sum siguren, veren otgovor

--select avg(numguns)
--from ships join classes on ships.class = classes.class
--where type = 'bb'

-- or
-- ne se prai taka

--select avg(numguns)
--from classes
--where type = 'bb'


-- 4

--select class, MIN(launched) as FirstYear, MAX(launched) as LastYear 
--from ships
--group by class

-- 5

--select class, count(ship) as NumberShips
--from outcomes join ships on ship = name
--where result='sunk'
--group by class

-- 6 

--select t1.class, SunkenShipsCount
--from
--(select class, count(name) as ShipsCount
--from ships
--group by class
--having count(name) >= 2 ) as t1
--join 
--(select class, count(ship) as SunkenShipsCount
--from outcomes join ships on name = ship
--where result='sunk'
--group by class) as t2
--on t1.class = t2.class

-- 7

--select country, convert(decimal(10, 2), AVG(bore)) as AvgBore
--from ships join classes on ships.class = classes.class
--group by country 


