USE MASTER;
-- CREATE DATABASE flights; 
USE flights;

-- 1 var 1 - constrints in definitions --
CREATE TABLE airline(
code CHAR(2) not null constraint pk_airline primary key,
name VARCHAR(20) not null constraint uq_airline_name unique,
country VARCHAR(20) not null
);
CREATE TABLE airport(
code CHAR(3) not null constraint pk_airport primary key,
name VARCHAR(20) not null,
country VARCHAR(20) not null,
city VARCHAR(20) not null,
constraint uq_airport uique(name, country)
);
CREATE TABLE airplane(
code CHAR(3) not null constraint pk_airplane primary key,
type VARCHAR(20) not null,
seats INT not null check(seats >0),
YEAR INT not null
);
CREATE TABLE flight(
fnumber VARCHAR(10) not null constraint pk_flight primary key,
airline_code CHAR(2) not null,
dep_airport_code CHAR(20) not null,
arr_airport_code CHAR(20) not null, 
f_time TIME null,
duration INT null,
airplane_code CHAR(3) null
);
CREATE TABLE customer(
id INT NOT NULL identity(1,1) constraint pk_customer primary key,
name VARCHAR(30) not null,
fname VARCHAR(30) not null,
email VARCHAR(30) not null
);
CREATE TABLE agency(
name VARCHAR(20) not null,
country VARCHAR(20) not null,
town VARCHAR(20) not null,
phone VARCHAR(15) null,
constraint pk_agency primary key(name, country, town)
);
CREATE TABLE booking(
code CHAR(6) not null constraint pk_booking primary key,
agency_name VARCHAR(20) not null,
airline_code CHAR(2) not null,
flight_number VARCHAR(10) not null,
customer_id INT not null,
booking_date DATE not null,
flight_date DATE not null,
price INT not null,
status BIT not null
);







-- 
-- CREATE TABLE airline(
-- code CHAR(2),
-- name VARCHAR(20),
-- country VARCHAR(20)
-- );
-- CREATE TABLE airport(
-- code CHAR(3),
-- name VARCHAR(20),
-- country VARCHAR(20),
-- city VARCHAR(20)
-- );
-- CREATE TABLE airplane(
-- code CHAR(3), TYPE VARCHAR(20),
-- seats INT, YEAR INT
-- );
-- CREATE TABLE flight(
-- fnumber VARCHAR(10),
-- airline_code CHAR(2),
-- dep_airport_code CHAR(20),
-- arr_airport_code CHAR(20), TIME TIME,
-- duration INT,
-- airplane-code CHAR(3)
-- );
-- CREATE TABLE customer(
-- id INT NOT NULL identity(1,1),
-- name VARCHAR(30),
-- fname VARCHAR(30),
-- email VARCHAR(30)
-- );
-- CREATE TABLE agency(
-- name VARCHAR(20),
-- country VARCHAR(20),
-- town VARCHAR(20),
-- phone VARCHAR(15)
-- );
-- CREATE TABLE booking(
-- code CHAR(6),
-- agency_name VARCHAR(20),
-- airline_code CHAR(2),
-- flight_number VARCHAR(10),
-- customer_id INT,
-- booking_date DATE,
-- flight_date DATE,
-- price INT, STATUS BIT
-- );
-- 